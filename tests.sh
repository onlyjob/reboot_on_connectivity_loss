#!/bin/sh

CMD="./reboot_on_connectivity_loss.sh"

test_bad_option() {
    ${CMD} -k    2>/dev/null
    assertTrue "[ $? != 0 ]"
}

test_no_endpoint() {
    ${CMD} -t 33    2>/dev/null
    assertTrue "[ $? != 0 ]"
}

. shunit2
