#!/bin/sh
: <<=cut

=head1 NAME

reboot_on_connectivity_loss.sh - detect connectivity loss and reboot

=head1 SYNOPSIS

reboot_on_connectivity_loss.sh [ B<-t {number_of_tries}> ] [ B<endpoint> ]

=head1 OPTIONS

=over 4

=item B<-t {number_of_tries}>

Number of attempts to establish connectivity.

Default is 30.

=back

=head1 DESCRIPTION

This script may be useful when it is necessary to reboot an appliance
when connectivity is lost. Whatever the trouble is (software bug or
hanging modem), this script reboots once given number of attempts to ping
the endpoint fail.

=head1 DEPENDENCIES

    logger   # bsdutils
    ping     # iputils-ping
    timeout  # coreutils

=head1 COPYRIGHT

Copyright: 2019 Libre Solutions Pty Ltd (https://raid6.com.au)
Copyright: 2019 Dmitry Smirnov <onlyjob@raid6.com.au>

=head1 LICENSE

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 Donations are much appreciated, please consider donating:

  https://raid6.com.au/~onlyjob/donate/

=head1 AVAILABILITY

  Home page : https://gitlab.com/onlyjob/reboot_on_connectivity_loss
     Script : https://gitlab.com/onlyjob/reboot_on_connectivity_loss/raw/master/reboot_on_connectivity_loss.sh

=head1 VERSION

2019.0208

=cut

usage () {
    printf "Usage:\n    %s [ -t {number_of_tries} ] [ endpoint ]\n" "$(basename "$0")" >&2
    exit 1
}

INTERVAL_SEC=33
TRIES=30

while [ -n "$1" ]; do
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        if [ "$(command -v perldoc)" ]; then
            perldoc "$0"
        else
            head -74 "$0"
        fi
        exit 0
    elif [ "$1" = "-t" ]; then
        shift
        TRIES="$1"
    elif [ "${1##-}" = "$1" ]; then
        ENDPOINT="$1"
    else
        printf "E: unknown option.\n" >&2
    fi
    shift
done

set -u

if [ 0 = "$((TRIES+0))" ]; then
    printf "E: {number_of_tries} should be a positive integer (e.g. 30).\n" >&2
    usage
fi

if [ -z "${ENDPOINT}" ]; then
    printf "E: endpoint is required.\n"
    usage
fi

if [ -z "$(command -v reboot)" ]; then
    printf "W: 'reboot' command is not available. Are you running this script as 'root'?\n" >&2
    printf "W: 'sudo reboot' will be tried...\n"                                            >&2
fi


tag="reboot_on_connectivity_loss"
ping_cmd="timeout 33 ping -c 1 ${ENDPOINT}"
TRY="${TRIES}" # initialize counter

printf "Trying to reach %s up to %s times with %s seconds interval.\n" "${ENDPOINT}" "${TRIES}" "${INTERVAL_SEC}"

while sleep "${INTERVAL_SEC}"; do
    if ${ping_cmd} 1>/dev/null 2>/dev/null; then
        if [ "${TRY}" != "${TRIES}" ]; then
            logger --tag "${tag}" --stderr "Connectivity restored; try counter reset to ${TRIES}."
        fi
        TRY="${TRIES}" # reset counter
        continue
    else
        TRY=$((TRY-1))
    fi
    logger --tag "${tag}" --stderr "${TRY} tries left until reboot."
    if [ "${TRY}" = 0 ]; then
        logger --tag "${tag}" --stderr "Rebooting..."
        reboot || sudo reboot
        break    # exit loop.
    fi
done
