#!/usr/bin/make -f
# -*- makefile -*-

S:=reboot_on_connectivity_loss.sh

default: test
check: test

.PHONY: default check test syntax bashisms shellcheck codespell podchecker shunit2

test: syntax bashisms shellcheck codespell podchecker shunit2

syntax:
	sh -n "$(S)"

bashisms:
	checkbashisms  "$(S)"

shellcheck:
	shellcheck -eSC1117 "$(S)"

codespell:
	codespell "$(S)"

podchecker:
	podchecker "$(S)"

shunit2:
	./tests.sh
